import { MockStoreEnhanced } from 'redux-mock-store';
import { {{pascalCase name}}DispatchMapper } from './{{pascalCase name}}DispatchMapper';
import { I{{pascalCase name}}DispatchProps } from './I{{pascalCase name}}DispatchProps';

let storeMock: MockStoreEnhanced;
let mappedProps: I{{pascalCase name}}DispatchProps;

beforeEach(() => {
    storeMock = Test.createMockStore();
    mappedProps = {{pascalCase name}}DispatchMapper.map(
        storeMock.dispatch
    );
});

describe(nameof<I{{pascalCase name}}DispatchProps>(p => p.PROP_NAME), () => {
    it('maps to dispatching ACTION_NAME', () => {
        mappedProps.FUNCTION_PROPS();
    
        expect(storeMock.getActions()).toContainEqual(ACTION_NAME);
    });
});

it.todo('add additional tests');
