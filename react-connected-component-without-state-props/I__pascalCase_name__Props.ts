import { I{{pascalCase name}}DispatchProps } from './I{{pascalCase name}}DispatchProps';
import { I{{pascalCase name}}OwnProps } from './I{{pascalCase name}}OwnProps';

export interface I{{pascalCase name}}Props
    extends I{{pascalCase name}}DispatchProps,
        I{{pascalCase name}}OwnProps {}
