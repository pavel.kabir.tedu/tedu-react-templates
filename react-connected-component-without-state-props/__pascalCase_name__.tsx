import React, { Component } from 'react';
import { StyleSheet } from 'react-native';
import { connect } from 'react-redux';
import { I{{pascalCase name}}DispatchProps } from './I{{pascalCase name}}DispatchProps';
import { I{{pascalCase name}}OwnProps } from './I{{pascalCase name}}OwnProps';
import { I{{pascalCase name}}Props } from './I{{pascalCase name}}Props';
import { {{pascalCase name}}DispatchMapper } from './{{pascalCase name}}DispatchMapper';

class {{pascalCase name}}Component extends Component <I{{pascalCase name}}Props> {
    public render() {
        return false;
    }
}

const styles = StyleSheet.create({

});

export const {{pascalCase name}} = connect <
    void,
    I{{pascalCase name}}DispatchProps,
    I{{pascalCase name}}OwnProps,
    IApplicationState
> (
    undefined,
    Test.wrapForLateMocking(() => {{pascalCase name}}DispatchMapper.map)
)(
    {{pascalCase name}}Component
);
