mocker.mockAllComponentsExcept(nameof({{pascalCase name}}));

import React from 'react';
import { {{pascalCase name}} } from './{{pascalCase name}}';
import { I{{pascalCase name}}OwnProps } from './I{{pascalCase name}}OwnProps';

const defaultOwnProps: Readonly<I{{pascalCase name}}OwnProps> = {

};

it('renders correctly', () => {
    const component = renderWithMockStore(
        <{{pascalCase name}} {...defaultOwnProps} />
    );

    expect(component).toMatchSnapshot();
});
