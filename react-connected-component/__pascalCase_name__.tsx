import React, { FunctionComponent } from 'react';
import { StyleSheet } from 'react-native';
import { connect } from 'react-redux';
import { I{{pascalCase name}}DispatchProps } from './I{{pascalCase name}}DispatchProps';
import { I{{pascalCase name}}OwnProps } from './I{{pascalCase name}}OwnProps';
import { I{{pascalCase name}}Props } from './I{{pascalCase name}}Props';
import { I{{pascalCase name}}StateProps } from './I{{pascalCase name}}StateProps';
import { {{pascalCase name}}DispatchMapper } from './{{pascalCase name}}DispatchMapper';
import { {{pascalCase name}}StateMapper } from './{{pascalCase name}}StateMapper';

const {{pascalCase name}}Component: FunctionComponent<I{{pascalCase name}}Props> = props => null;

const styles = StyleSheet.create({

});

export const {{pascalCase name}} = connect <
    I{{pascalCase name}}StateProps,
    I{{pascalCase name}}DispatchProps,
    I{{pascalCase name}}OwnProps,
    IApplicationState
> (
    Test.wrapForLateMocking(() => {{pascalCase name}}StateMapper.map),
    Test.wrapForLateMocking(() => {{pascalCase name}}DispatchMapper.map)
)(
    {{pascalCase name}}Component
);
