import { I{{pascalCase name}}DispatchProps } from './I{{pascalCase name}}DispatchProps';
import { I{{pascalCase name}}OwnProps } from './I{{pascalCase name}}OwnProps';
import { I{{pascalCase name}}StateProps } from './I{{pascalCase name}}StateProps';

export interface I{{pascalCase name}}Props
    extends I{{pascalCase name}}DispatchProps,
        I{{pascalCase name}}StateProps,
        I{{pascalCase name}}OwnProps {}
