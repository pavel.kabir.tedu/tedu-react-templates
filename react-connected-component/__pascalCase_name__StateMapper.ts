import { I{{pascalCase name}}StateProps } from './I{{pascalCase name}}StateProps';
import { I{{pascalCase name}}OwnProps } from './I{{pascalCase name}}OwnProps';

export class {{pascalCase name}}StateMapper {
    public static map = (
        state: IApplicationState,
        ownProps: I{{pascalCase name}}OwnProps
    ): I{{pascalCase name}}StateProps => ({

    });
}
