import { Dispatch } from 'redux';
import { I{{pascalCase name}}DispatchProps } from './I{{pascalCase name}}DispatchProps';

export class {{pascalCase name}}DispatchMapper {
    public static map = (
        dispatch: Dispatch<ApplicationActionType>
    ): I{{pascalCase name}}DispatchProps => ({
        
    });
}
