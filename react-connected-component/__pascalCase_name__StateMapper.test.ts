import { {{pascalCase name}}StateMapper } from './{{pascalCase name}}StateMapper';
import { I{{pascalCase name}}OwnProps } from './I{{pascalCase name}}OwnProps';
import { I{{pascalCase name}}StateProps } from './I{{pascalCase name}}StateProps';

const defaultOwnProps: Readonly<I{{pascalCase name}}OwnProps> = {

};

describe(nameof<I{{pascalCase name}}StateProps>(p => p.PROP_NAME), () => {
    it('maps props as empty object', () => {
        const props = {{pascalCase name}}StateMapper.map(InitialApplicationState, defaultOwnProps);
    
        expect(props.PROP_NAME).toEqual({});
    });    
});

it.todo('add additional tests for every prop field');
