import { ActionType } from 'typesafe-actions';
import * as {{camelCase name}}Actions from './{{pascalCase name}}Actions';

export type {{pascalCase name}}ActionType = ActionType<typeof {{camelCase name}}Actions>;

// Add {{pascalCase name}}ActionType to ApplicationActionType.ts
