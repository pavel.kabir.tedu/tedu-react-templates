import { Initial{{pascalCase name}}State } from './Initial{{pascalCase name}}State';
import { I{{pascalCase name}}State } from './I{{pascalCase name}}State';
import * as {{camelCase name}}Actions from './{{pascalCase name}}Actions';
import { {{pascalCase name}}Reducer } from './{{pascalCase name}}Reducer';
import { Test } from '../../modules/Test';

const createState = (values: Partial<I{{pascalCase name}}State>): I{{pascalCase name}}State => ({
    ...Initial{{pascalCase name}}State,
    ...values,
});




describe('default', () => {
    it('returns the provided state upon an irrelevant action', () => {
        const oldState = createState({
            
        });

        const newState = {{pascalCase name}}Reducer(oldState, Test.irrelevantActionSingleton);

        expect(newState).toBe(oldState);
    });

    it('returns the initial state when no explicit state is provided', () => {
        const newState = {{pascalCase name}}Reducer(undefined, Test.irrelevantActionSingleton);
    
        expect(newState).toBe(Initial{{pascalCase name}}State);
    });
});
