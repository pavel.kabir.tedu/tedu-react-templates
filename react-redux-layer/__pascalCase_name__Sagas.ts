import * as effects from 'redux-saga/effects';
import { getType } from 'typesafe-actions';
import * as {{camelCase name}}Actions from './{{pascalCase name}}Actions';


export function* root{{pascalCase name}}Saga() {
    yield effects.all([
        
    ]);
}

// Add root{{pascalCase name}}Saga to RootSaga.ts
