import { IApplicationState } from '../IApplicationState';
import { InitialApplicationState } from '../InitialApplicationState';
import { I{{pascalCase name}}State } from './I{{pascalCase name}}State';
import { {{pascalCase name}}Selectors } from './{{pascalCase name}}Selectors';

const createState = (values: Partial<I{{pascalCase name}}State>): IApplicationState => ({
    ...InitialApplicationState,
    {{camelCase name}}: {
        ...InitialApplicationState.{{camelCase name}},
        ...values,
    },
});
