import { Reducer } from 'redux';
import { getType } from 'typesafe-actions';
import { Initial{{pascalCase name}}State } from './Initial{{pascalCase name}}State';
import { I{{pascalCase name}}State } from './I{{pascalCase name}}State';
import { Test } from '../../modules/Test';
import * as {{camelCase name}}Actions from './{{pascalCase name}}Actions';
import { {{pascalCase name}}ActionType } from './{{pascalCase name}}ActionType';

export const {{pascalCase name}}Reducer: Reducer<I{{pascalCase name}}State, {{pascalCase name}}ActionType> = (
    state = Initial{{pascalCase name}}State,
    action: {{pascalCase name}}ActionType
): I{{pascalCase name}}State => {

    return state;
};

// Add {pascalCase name}}Reducer to ApplicationReducer.ts
