import { IApplicationState } from '../IApplicationState';
import { I{{pascalCase name}}State } from './I{{pascalCase name}}State';

export class {{pascalCase name}}Selectors {
    private static readonly get{{pascalCase name}}State = (
        state: IApplicationState
    ): I{{pascalCase name}}State => state.{{camelCase name}};
}
