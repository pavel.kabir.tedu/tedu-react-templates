import { expectSaga } from 'redux-saga-test-plan';
import * as effects from 'redux-saga/effects';
import * as matchers from 'redux-saga-test-plan/matchers';
import * as providers from 'redux-saga-test-plan/providers';
import { assertSaga } from 'redux-saga-assert';
import { Test } from '../../modules/Test';
import * as {{camelCase name}}Actions from './{{pascalCase name}}Actions';
import * as sagas from './{{pascalCase name}}Sagas';

describe(nameof(sagas.root{{pascalCase name}}Saga), () => {
    it('spawns all the {{lowerCase name}} watcher sagas', async () =>
        assertSaga(sagas.root{{pascalCase name}}Saga).justSpawnsAsync(
        ));
});
