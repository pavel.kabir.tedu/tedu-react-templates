import React, { FunctionComponent } from 'react';
import { StyleSheet } from 'react-native';
import { I{{pascalCase name}}Props } from './I{{pascalCase name}}Props';

export const {{pascalCase name}}: FunctionComponent<I{{pascalCase name}}Props> = props => null;

const styles = StyleSheet.create({

});
