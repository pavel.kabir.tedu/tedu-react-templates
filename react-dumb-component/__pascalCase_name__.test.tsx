mocker.mockAllComponentsExcept(nameof({{pascalCase name}}));

import React from 'react';
import renderer from 'react-test-renderer';
import { {{pascalCase name}} } from './{{pascalCase name}}';

it('renders correctly', () => {
    const component = renderer.create(
        <{{pascalCase name}} />
    );

    expect(component).toMatchSnapshot();
});
