mocker.mockAllComponentsExcept(nameof({{pascalCase name}}));

import React from 'react';
import { {{pascalCase name}} } from './{{pascalCase name}}';
import { {{pascalCase name}}StateMapper } from './{{pascalCase name}}StateMapper';
import { I{{pascalCase name}}StateProps } from './I{{pascalCase name}}StateProps';
import { I{{pascalCase name}}OwnProps } from './I{{pascalCase name}}OwnProps';

let {{camelCase name}}StateMapperMocks: FunctionsReplacedWithMocks<typeof {{pascalCase name}}StateMapper>;

const defaultStateProps: Readonly<I{{pascalCase name}}StateProps> = {

};

const defaultOwnProps: Readonly<I{{pascalCase name}}OwnProps> = {

};

beforeEach(() => {
    {{camelCase name}}StateMapperMocks = mocker.mockAllFunctionsInObject({{pascalCase name}}StateMapper);

    {{camelCase name}}StateMapperMocks.map.mockReturnValue(defaultStateProps)
});


it('renders correctly', () => {
    const component = renderWithMockStore(
        <{{pascalCase name}} {...defaultOwnProps} />
    );

    expect(component).toMatchSnapshot();
});
