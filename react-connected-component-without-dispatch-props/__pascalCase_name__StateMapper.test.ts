import { {{pascalCase name}}StateMapper } from './{{pascalCase name}}StateMapper';
import { I{{pascalCase name}}OwnProps } from './I{{pascalCase name}}OwnProps';

const defaultOwnProps: Readonly<I{{pascalCase name}}OwnProps> = {

};

describe(nameof<I{{pascalCase name}}StateProps>(p => p.FIELD_NAME), () => {
    it(`maps as the empty object`, () => {
        const props = {{pascalCase name}}StateMapper.map(InitialApplicationState, defaultOwnProps);
    
        expect(props).toEqual({});
    });
}

it.todo('add additional tests for every prop field');
