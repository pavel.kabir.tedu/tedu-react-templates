import React, { Component } from 'react';
import { StyleSheet } from 'react-native';
import { connect } from 'react-redux';
import { I{{pascalCase name}}OwnProps } from './I{{pascalCase name}}OwnProps';
import { I{{pascalCase name}}Props } from './I{{pascalCase name}}Props';
import { I{{pascalCase name}}StateProps } from './I{{pascalCase name}}StateProps';
import { {{pascalCase name}}StateMapper } from './{{pascalCase name}}StateMapper';

class {{pascalCase name}}Component extends Component <I{{pascalCase name}}Props> {
    public render() {
        return false;
    }
}

const styles = StyleSheet.create({

});

export const {{pascalCase name}} = connect <
    I{{pascalCase name}}StateProps,
    void,
    I{{pascalCase name}}OwnProps,
    IApplicationState
> (
    Test.wrapForLateMocking(() => {{pascalCase name}}StateMapper.map)
)(
    {{pascalCase name}}Component
);
